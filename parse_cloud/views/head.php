<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home</title>
	<meta charset="utf-8" />
	<meta name = "format-detection" content = "telephone=no" />
	<link rel="icon" href="views/images/favicon.ico" />
	<link rel="shortcut icon" href="views/images/favicon.ico" />
	<link rel="stylesheet" href="views/css/stuck.css" />
	<link rel="stylesheet" href="views/css/style.css" />
	<link rel="stylesheet" href="views/css/touchTouch.css" />
	<script src="views/js/jquery.js"></script>
	<script src="views/js/jquery-migrate-1.1.1.js"></script>
	<script src="views/js/script.js"></script> 
	<script src="views/js/superfish.js"></script>
	<script src="views/js/jquery.equalheights.js"></script>
	<script src="views/js/jquery.mobilemenu.js"></script>
	<script src="views/js/jquery.easing.1.3.js"></script>
	<script src="views/js/tmStickUp.js"></script>
	<script src="views/js/jquery.ui.totop.js"></script>
	<script src="views/js/touchTouch.jquery.js"></script>

	<script>
		$(document).ready(function(){
			$().UItoTop({ easingType: 'easeOutQuart' });
			$('#stuck_container').tmStickUp({});
			$('.gallery .gall_item').touchTouch();
		}); 
	</script>
	<!--[if lt IE 9]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		</a>
	</div>
	<script src="js/html5shiv.js"></script>
	<link rel="stylesheet" media="screen" href="css/ie.css">


	<![endif]-->
	<script type="text/javascript" src="angular.min.js"></script>
</head>