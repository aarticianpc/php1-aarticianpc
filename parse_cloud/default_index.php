<!DOCTYPE html>
<?php include('views/head.php'); ?>
<body class="page1" id="top" data-ng-app="myApp" data-ng-controller="myCltr">
	<?php include('views/header.php'); ?>
	<section class="content">
		<div class="container">
			<div class="row" data-ng-show="result">
				<div class="grid_12">	
					<h2>MENU ITEMS</h2>
					<ul>
						<li data-ng-repeat="x in items">{{x.item_name}}</li>
					</ul>
				</div>	
			</div>	
			<div class="row" data-ng-hide="result">
				<div class="grid_12">	
					<a href="javascript:void(0);" class="btn" data-ng-click="loadData();">Generate</a>
				</div>	
			</div>	
		</div>
	</section>
	<?php include('views/footer.php'); ?>
	
	<script type="text/javascript">
		var app = angular.module('myApp', []);
		app.controller('myCltr', function($scope, $http){
			$scope.result = false;
			$scope.loadData = function() {
				$http.get("default_parse.php").success( function(response) {
					$scope.items = response.records;
					$scope.result = true;
				});
			}
		});
	</script>
	
</body>
</html>	